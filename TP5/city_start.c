#include<stdio.h>
#include<stdlib.h>

typedef struct City City;
struct City {
    char *name; // ou pourrait envisager char name[50]
    int pop;
    float area;
    City *next;
};

void print_city(City *city) {
    printf("Ville de %s\n", city->name);
    printf("  Population : %d habitants\n", city->pop); 
    printf("  Surface : %.2f km²\n", city->area); 
}

City *read_city() {
    City *city;

    city = malloc(sizeof(City));

    printf("Nom de la ville (pas d'espace dans les noms) : ");
    scanf("%ms", &(city->name));
    printf("Population : ");
    scanf("%d", &(city->pop));
    printf("Surface : ");
    scanf("%f", &(city->area));
    city->next = NULL;

    return city;
}

void print_all_cities(City *head) {
    City *current;

    current = head;
    while( current ) {
        print_city(current);
        current = current->next; 
    }
}

City *new_city(char *name, int pop, float area) {
    City *city;
    city = malloc(sizeof(City));
    city->name = name;
    city->pop = pop;
    city->area = area;
    city->next = NULL;
}

City *insert_city(City *head, char *name, int pop, float area) {
    City *current;

    current = head;

    // deux cas  possible : la liste est vide (head est NULL)
    // ou pas...
    if ( head == NULL ) {
        head = new_city(name, pop, area);
    } else { // la liste n'est pas vide
        current = head;
        while ( current->next ) {
            current = current->next;
        }
        // arrivé ici on est sur un noeud qui n'a pas de successeur
        current->next = new_city(name, pop, area);
    }

    return head;
}
        
City *insert_city_node(City *head, City *new) {
    City *current;

    current = head;

    // deux cas  possible : la liste est vide (head est NULL)
    // ou pas...
    if ( head == NULL ) {
        head = new;
    } else { // la liste n'est pas vide
        current = head;
        while ( current->next ) {
            current = current->next;
        }
        // arrivé ici on est sur un noeud qui n'a pas de successeur
        current->next = new;
    }

    return head;
}
    

int main() {
    int N;
    City *head = NULL;

    // head est mis à jour seulement lors du premier appel
    head = insert_city(head, "Versailles", 83583, 26.48);
    head = insert_city(head, "Montpellier", 299006, 56.88);
    head = insert_city(head, "Morlaix", 14709, 24.82);

    // head = insert_city_node(head, read_city());

    print_all_cities(head); 

    exit(0);
}
