#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int main() {
	srand(time(NULL));
	int alea = rand()%101, idee, compt = 1;
	printf("Essayez de deviner  un nombre entre 1 et 100\n");
	printf("Votre idée : ");
	scanf("%d", &idee);
	while (alea != idee) {
		if (alea > idee) {
			printf("Trop petit...\n");
			printf("Votre idée : ");
			scanf("%d", &idee);
		}
		if (alea < idee) {
			printf("Trop grand...\n");
                        printf("Votre idée : ");
                        scanf("%d", &idee);
		}
		compt++;
	}
	printf("Gagné ! en ");
	printf("%d", compt + 1);
	printf(" coups !\n");
	printf("Bravo !\n");
}
