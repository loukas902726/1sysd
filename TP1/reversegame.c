#include <stdio.h>
#include <stdlib.h>

int main() {
	int min = 1, max = 100, notfound = 1, compt = 0;
	char ans;
    	printf("Pensez à un nombre entre 1 et 100.\n");
	while (notfound) {
		int guess = (max + min) / 2;
		printf("Est-ce que le nombre est %d \n", guess);
		printf("+ (si trop petit), - (si trop grand) ou = : ");
		scanf(" %c", &ans);
		if (ans == '=') {
			notfound = 0;
		} else if (ans == '+') {
			min = guess + 1;
		} else if (ans == '-') {
			max = guess;
		} else {
			printf("Veuillez entrer un caractère valide\n");
		}
		compt++;
	}
	printf("J'ai trouvé le nombre en %d coups.\n", compt);
}
