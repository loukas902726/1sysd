#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

int count_char(char s1[], char s2[], int ignore) {
    char *p, *q;
    p = s1;
    q = s2;
    int l = 0;
    while (*p) {
	if ((ignore && tolower(*p) == tolower(*q)) || (!ignore && *p == *q)) {
		l++;
	}
        p++;
    }
    return l;
}

int main(int argc, char *argv[]) {
    printf("Nb d'arguments : %d\n", argc);
    int ignore = 0;
        if (strcmp(argv[3], "-i") == 0) {
            ignore = 1;
        }
    printf("Le nombre de caractère identique est : %d\n", count_char(argv[1], argv[2], ignore));
    for (int i = 0; i < argc; i++) {
        printf("%s\n", argv[i]);
    }
    return 0;
}
