#include<stdio.h>
#include<stdio_ext.h>
#include<stdlib.h>

int count_char(char s1[], char s2[]) {
    char *p, *q;
    p = s1;
    q = s2;
    int l = 0;
    while (*p) {
	if (*p == *q) {
		l++;
	}
        p++;
    }
    return l;
}

int main(int argc, char *argv[]) {
    printf("Nb d'arguments : %d\n", argc);
    printf("Le nombre de caractère identique est : %d\n", count_char(argv[1], argv[2]));
    for (int i = 0; i < argc; i++) {
        printf("%s\n", argv[i]);
    }
    exit(1);
}
