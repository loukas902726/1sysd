#include<stdlib.h>
#include<stdio.h>

float celsius2fahrenheit(float celsius) {
    float fahrenheit = (9.0 / 5.0) * celsius + 32.0;
    return fahrenheit;
}

float fahrenheit2celsius(float fahrenheit) {
    float celsius = (fahrenheit - 32.0) * 5.0 / 9.0;
    return celsius;
}

int main() {
    int choix;
    float temp;
    printf("si vous souhaitez convertir de °C à °F tapez 1 ou 2 si vous souhaitez l'inverse : \n");
    scanf("%d", &choix);

    if (choix == 1) {
        printf("Entrez la température en degrés Celsius : ");
        scanf("%f", &temp);
        printf("%.2f °C correspond à %.2f °F\n", temp, celsius2fahrenheit(temp));
    }
    else if (choix == 2) {
        printf("Entrez la température en degrés Fahrenheit : ");
        scanf("%f", &temp);
        printf("%.2f °F correspond à %.2f °C\n", temp, fahrenheit2celsius(temp));
    }
    else {
        printf("Veuillez saisir 1 ou 2.\n");
    }

    exit(0);
}
