#include<stdio.h>
#include<stdlib.h>

typedef struct City City;
struct City {
    char *name; // ou pourrait envisager char name[50]
    int pop;
    float area;
};

City read_city() {
    City city;

    printf("Nom de la ville (pas d'espace dans les nom) : ");
    scanf("%ms", &city.name);
    printf("Population : ");
    scanf("%d", &city.pop);
    printf("Surface : ");
    scanf("%f", &city.area);

    return city;
}


void print_city(City c) {

	printf("Ville de %s\n", c.name);
        printf("  Population : %d habitants\n", c.pop); 
        printf("  Surface : %.2f km²\n", c.area);
}

int main() {
    City cities[50];
    int n, i;
    printf("Combiens de villes voulez-vous : ");
    scanf("%d", &n);
    for ( i = 0 ; i < n ; i++) {
	cities[i] = read_city();
    	print_city(cities[i]);
    }

    exit(0);
}
 
