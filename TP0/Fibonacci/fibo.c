#include <stdio.h>

int main() {
	int n, un, u0 = 0, u1 = 1, i;
	printf("Entrez un nombre : ");
	scanf("%d", &n);
	if (n < 2) {
		printf("%d\n", n);
	} else {
		for (i = 1; i <= n; i++) {
			un = u0 + u1;
			u0 = u1;
			u1 = un;
			printf("%d\n", un);
		}
	}
}
