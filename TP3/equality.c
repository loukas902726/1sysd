#include<stdio.h>
#include<stdlib.h>

int mystrscanf(char **s) {
    // You are not supposed to understand this function
    static char empty_string[] = { '\0' };
    int ret;

    ret = scanf("%m[^\n]", s);    // non-std, extension GNU
                                   // de scanf : alloue la mémoire
    // si chaîne vide (<entrée>) : pointeur vaut NULL
    if ( *s == NULL ) {
        *s = empty_string;
    }
    
    return ret;
}

int slength(char s[]) {
    int l = 0;
    char *p;
    p = s;
    while ( *p++ ) { // tant qu'on est pas sur le zéro final...
        l++;         // on augmente l
    }
    return l;
}

int is_upper(char s[]) {
    char *p;
    int up = 1;
    for ( p = s ; *p && up ; p++ ) {
        if ( *p >= 'a' && *p <= 'z' ) {
            up = 0;
        }
    }
    return up;
}

int is_upper2(char s[]) {
    char *p;
    int up = 1;
    p = s;
    while (*p && up) {
        if ( *p >= 'a' && *p <= 'z' ) {
            up = 0;
        }
        p++;
    }
    return up;
}

int sequal(char s[], char y[]) {
	char *p;
	char *pp;
	p = s;
	pp = y;
	int up = 1;
	while (*p && *pp) {
		if (*p != pp) {
			up = 0;
			break;
		}
	p++;
	pp++;
	}
}

int main() {
    // pas de taille max car on utilise GNU scanf... 
    // cf. plus bas et man scanf
    char *texte, *texte2; 

    printf("Entrez une chaîne : ");
    mystrscanf(&texte);
    printf("Entrez encore une chaîne : ");
    mystrscanf(&texte2);

    printf("Longueur de la première chaîne : %d\n", slength(texte));
    if (is_upper2(texte)) {
        printf("La première chaîne est entièrement en capitales.\n");
    } else {
        printf("La chaîne n'est pas entièrement en capitales.\n");
    }
    if (sequal(texte, texte2)) {
        printf("Les deux chaînes sont identiques.\n");
    } else {
        printf("Les deux chaînes sont différentes.\n");
    }
    exit(0);
}

