
#include<stdio.h>
#include<stdlib.h>

int main() {
    char texte[100]; // chaîne de taille <= 999 (zéro terminal !)
    int i;

    printf("Tapez une phrase : "); 
    scanf("%[^\n]", texte);
    printf("Phrase : %s\n", texte);

    i = 0;
    int  n = 0;
    while (texte[i] == ' ') {
    	i++;
    }
    while (texte[i] != '\0') { // ou != 0
	n++;
	while (texte[i] != ' ' && texte[i] != '\0') {
		i++;
	}
	while (texte[i] == ' ') {
		i++;
	}
    }
    printf("\nLongueur de la chaîne : %d\n", i);
    printf("\nNombre de mot : %d\n", n);
    exit(0);
}
