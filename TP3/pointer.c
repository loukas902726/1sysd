#include<stdio.h>
#include<stdlib.h>

int main() {
    int *p, n = 42;

    p = &n;
    printf("%d\n", *p); // 42
    printf("%d\n", n);  // 42
    printf("%d\n", *(&n)); // 42
    printf("%lu\n", (unsigned long int)&p); // ???
    printf("%lu\n", (unsigned long int)&n); // ??? (différent du précédent)
    printf("%lu\n", (unsigned long int)&(*p)); // ??? (idem précédent)
    exit(0);
}

