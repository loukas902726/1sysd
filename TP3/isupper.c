#include<stdio.h>
#include<stdlib.h>

int mystrscanf(char **s) {
    // You are not supposed to understand this function
    static char empty_string[] = { '\0' };
    int ret;

    ret = scanf("%m[^\n]", s);    // non-std, extension GNU
                                   // de scanf : alloue la mémoire
    // si chaîne vide (<entrée>) : pointeur vaut NULL
    if ( *s == NULL ) {
        *s = empty_string;
    }
    
    return ret;
}

int true = 0;

int isupper(char s[]) {
	char *p;
	p = s;
	while (*p) {
                if (*p >= 'a' && *p <= 'z') {
		true = 0;
                break;
		}
	true = 1;
	p++;
        }
        return true;
}

int main() {
    char *texte; // pas de taille max car on utilise GNU scanf... 
                 // cf. plus bas et man scanf

    printf("Entrez une chaîne : ");
    mystrscanf(&texte);
    isupper(texte);
    printf("Capitalisation : %d\n", true);
    exit(0);
}
