#include<stdio.h>
#include<stdlib.h>

int mystrscanf(char **s) {
    // You are not supposed to understand this function
    static char empty_string[] = { '\0' };
    int ret;

    ret = scanf("%m[^\n]", s);    // non-std, extension GNU
                                   // de scanf : alloue la mémoire
    // si chaîne vide (<entrée>) : pointeur vaut NULL
    if ( *s == NULL ) {
        *s = empty_string;
    }
    
    return ret;
}

int slength(char s[]) {
    int l = 0;
    char *p;
    p = s;
    while ( *p++ ) { // tant qu'on est pas sur le zéro final...
        l++;         // on augmente l
    }
    return l;
}

int main() {
    char *texte; // pas de taille max car on utilise GNU scanf... 
                 // cf. plus bas et man scanf

    printf("Entrez une chaîne : ");
    mystrscanf(&texte);
    printf("Longueur : %d\n", slength(texte));
    exit(0);
}
